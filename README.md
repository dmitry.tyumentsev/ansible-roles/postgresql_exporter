postgresql exporter role
=========

Installs postgresql exporter for Prometheus on Debian/Ubuntu servers.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.postgresql_exporter
```
